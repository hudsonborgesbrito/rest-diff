package br.com.hudson.diff.rest;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import br.com.hudson.diff.app.Application;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=Application.class)
@AutoConfigureMockMvc
public class DiffRestControllerTest {
	
	@Autowired
    private MockMvc mvc;
	
	@Test
	public void testAddLeftValue_ValidRequest() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/v1/diff/123/left").content(Base64.encodeBase64("LEFT VALUE".getBytes())))
			.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void testAddLeftValue_InvalidRequest() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/v1/diff/123/left").content("LEFT@---+!VALUE"))
			.andExpect(MockMvcResultMatchers.status().is4xxClientError());
	}
}
