package br.com.hudson.diff.service;

import org.apache.commons.codec.binary.Base64;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.hudson.diff.constants.DiffConstants;
import br.com.hudson.diff.domain.DiffValue;
import br.com.hudson.diff.dto.DiffResult;
import br.com.hudson.diff.repository.DiffRepository;

@RunWith(MockitoJUnitRunner.class)
public class DiffServiceTest {

	@InjectMocks
	private DiffService diffService;

	@Mock
	private DiffRepository diffRepositoryMock;
	
	private static String validId = "TEST_ID";
	private static String plainTextValue = "@SPR!NG_B00T_ROCK!";
	
	@Test(expected = IllegalArgumentException.class)
	public void testAddOrUpdateLeftValue_NullID() {
		diffService.addOrUpdateLeftValue(null, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddOrUpdateLeftValue_NullValue() {
		diffService.addOrUpdateLeftValue(validId, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddOrUpdateLeftValue_InvalidValue() {
		diffService.addOrUpdateLeftValue(validId, plainTextValue);
	}

	@Test
	public void testAddOrUpdateLeftValue_ValidValue() {
		Mockito.doNothing().when(diffRepositoryMock).addOrUpdateLeft(Mockito.anyString(), Mockito.anyString());
		diffService.addOrUpdateLeftValue(validId, Base64.encodeBase64String(plainTextValue.getBytes()));
		Mockito.verify(diffRepositoryMock).addOrUpdateLeft(Mockito.anyString(), Mockito.anyString());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddOrUpdateRightValue_NullID() {
		diffService.addOrUpdateRightValue(null, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddOrUpdateRightValue_NullValue() {
		diffService.addOrUpdateRightValue(validId, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAddOrUpdateRightValue_InvalidValue() {
		diffService.addOrUpdateRightValue(validId, plainTextValue);
	}

	@Test
	public void testAddOrUpdateRightValue_ValidValue() {
		Mockito.doNothing().when(diffRepositoryMock).addOrUpdateRight(Mockito.anyString(), Mockito.anyString());
		diffService.addOrUpdateRightValue(validId, Base64.encodeBase64String(plainTextValue.getBytes()));
		Mockito.verify(diffRepositoryMock).addOrUpdateRight(Mockito.anyString(), Mockito.anyString());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testDiffValues_InvalidId() {
		diffService.diffValues("MY_ID");
	}
	
	@Test
	public void testDiffValues_EqualText() {
		DiffValue dv = new DiffValue(Base64.encodeBase64String("VALUE".getBytes()),Base64.encodeBase64String("VALUE".getBytes()));
		Mockito.doReturn(dv).when(diffRepositoryMock).findById(Mockito.anyString());
		DiffResult dr = this.diffService.diffValues("ID");
		
		Assert.assertNotNull(dr);
		Assert.assertEquals(DiffConstants.SIZE_LEFT_EQUAL_RIGHT, dr.getSizeResultMessage());
		Assert.assertEquals(DiffConstants.CONTENT_LEFT_EQUAL_RIGHT, dr.getValueMatchResult());
	}
	
	@Test
	public void testDiffValues_LeftSizeHigherSize() {
		DiffValue dv = new DiffValue(Base64.encodeBase64String("VALUE".getBytes()),Base64.encodeBase64String("VAL".getBytes()));
		Mockito.doReturn(dv).when(diffRepositoryMock).findById(Mockito.anyString());
		DiffResult dr = this.diffService.diffValues("ID");
		
		Assert.assertNotNull(dr);
		Assert.assertEquals(DiffConstants.SIZE_LEFT_HIGHER_RIGHT, dr.getSizeResultMessage());
	}
	@Test
	public void testDiffValues_RightSizeHigherSize() {
		DiffValue dv = new DiffValue(Base64.encodeBase64String("VAL".getBytes()),Base64.encodeBase64String("VALUE".getBytes()));
		Mockito.doReturn(dv).when(diffRepositoryMock).findById(Mockito.anyString());
		DiffResult dr = this.diffService.diffValues("ID");
		
		Assert.assertNotNull(dr);
		Assert.assertEquals(DiffConstants.SIZE_LEFT_LOWER_RIGHT, dr.getSizeResultMessage());
	}
	
	@Test
	public void testDiffValues_SameSize_DifferentText() {
		DiffValue dv = new DiffValue(Base64.encodeBase64String("VALUE LEFT SIZE".getBytes()),Base64.encodeBase64String("VALUE THIS SIZE".getBytes()));
		Mockito.doReturn(dv).when(diffRepositoryMock).findById(Mockito.anyString());
		DiffResult dr = this.diffService.diffValues("ID");
		
		Assert.assertNotNull(dr);
		Assert.assertEquals(DiffConstants.SIZE_LEFT_EQUAL_RIGHT, dr.getSizeResultMessage());
		Assert.assertEquals(DiffConstants.CONTENT_LEFT_NOT_EQUAL_RIGHT, dr.getValueMatchResult());
	}
	
	
}
