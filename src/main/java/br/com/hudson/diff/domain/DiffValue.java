package br.com.hudson.diff.domain;

public class DiffValue {

	private String leftValue;
	private String rightValue;

	public DiffValue() {
		super();
	}

	public DiffValue(String leftValue, String rightValue) {
		this.leftValue = leftValue;
		this.rightValue = rightValue;
	}

	public String getLeftValue() {
		return leftValue;
	}

	public void setLeftValue(String leftValue) {
		this.leftValue = leftValue;
	}

	public String getRightValue() {
		return rightValue;
	}

	public void setRightValue(String rightValue) {
		this.rightValue = rightValue;
	}

}
