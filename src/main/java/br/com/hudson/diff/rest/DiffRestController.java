package br.com.hudson.diff.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.hudson.diff.dto.DiffResult;
import br.com.hudson.diff.service.DiffService;

@RestController
public class DiffRestController {
	
	@Autowired
	private DiffService diffService;
	
	@PostMapping("/v1/diff/{id}/left")
	public ResponseEntity<Object> addLeftValue(@PathVariable("id") String id, @RequestBody String value){
		try {
			diffService.addOrUpdateLeftValue(id, value);
			return ResponseEntity.ok().build();
		}catch(IllegalArgumentException iae) {
			return ResponseEntity.badRequest().body(iae.getMessage());
		}
	}
	
	@PostMapping("/v1/diff/{id}/right")
	public ResponseEntity<Object> addRightValue(@PathVariable("id") String id, @RequestBody String value){
		try {
			diffService.addOrUpdateRightValue(id, value);
			return ResponseEntity.ok().build();
		}catch(IllegalArgumentException iae) {
			return ResponseEntity.badRequest().body(iae.getMessage());
		}
	}
	
	@GetMapping("/v1/diff/{id}")
	public ResponseEntity<Object> diff(@PathVariable("id") String id){
		try {
			DiffResult response = diffService.diffValues(id);
			return ResponseEntity.ok(response);
		}catch(IllegalArgumentException iae) {
			return ResponseEntity.badRequest().body(iae.getMessage());
		}
	}
}
