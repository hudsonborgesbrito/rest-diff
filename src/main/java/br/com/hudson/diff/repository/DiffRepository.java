package br.com.hudson.diff.repository;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Repository;

import br.com.hudson.diff.domain.DiffValue;

@Repository
public class DiffRepository {

	private Map<String, DiffValue> values;

	@PostConstruct
	public void init() {
		this.values = new HashMap<>();
	}

	public void addOrUpdateLeft(String id, String leftValue) {
		DiffValue diff = findById(id);
		if (diff == null) {
			diff = new DiffValue();
		}
		diff.setLeftValue(leftValue);
		values.put(id, diff);
	}

	public void addOrUpdateRight(String id, String rightValue) {
		DiffValue diff = findById(id);
		if (diff == null) {
			diff = new DiffValue();
		}
		diff.setRightValue(rightValue);
		values.put(id, diff);
	}

	public DiffValue findById(String id) {
		return values.get(id);
	}

}
