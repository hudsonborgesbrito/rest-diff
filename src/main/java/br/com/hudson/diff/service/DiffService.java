package br.com.hudson.diff.service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch.Diff;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.hudson.diff.constants.DiffConstants;
import br.com.hudson.diff.domain.DiffValue;
import br.com.hudson.diff.dto.DiffResult;
import br.com.hudson.diff.repository.DiffRepository;

@Service
public class DiffService {

	@Autowired
	private DiffRepository diffRepository;

	protected void validateId(String id) {
		if (StringUtils.isEmpty(id)) {
			throw new IllegalArgumentException("Invalid ID");
		}
	}

	protected void validateInput(String id, String value) {
		validateId(id);
		if (StringUtils.isEmpty(value) || !Base64.isBase64(value)) {
			throw new IllegalArgumentException("Invalid value");
		}
	}

	public void addOrUpdateLeftValue(String id, String value) {
		validateInput(id, value);
		diffRepository.addOrUpdateLeft(id, value);
	}

	public void addOrUpdateRightValue(String id, String value) {
		validateInput(id, value);
		diffRepository.addOrUpdateRight(id, value);
	}

	public DiffResult diffValues(String id) {
		validateId(id);

		DiffValue diffValue = diffRepository.findById(id);
		if (diffValue == null) {
			throw new IllegalArgumentException("Invalid ID");
		}

		DiffResult dr = new DiffResult();

		if (diffValue.getLeftValue().length() == diffValue.getRightValue().length()) {
			dr.setSizeResultMessage(DiffConstants.SIZE_LEFT_EQUAL_RIGHT);
			this.processDiffString(diffValue, dr);
		} else if (diffValue.getLeftValue().length() < diffValue.getRightValue().length()) {
			dr.setSizeResultMessage(DiffConstants.SIZE_LEFT_LOWER_RIGHT);
		} else {
			dr.setSizeResultMessage(DiffConstants.SIZE_LEFT_HIGHER_RIGHT);
		}

		return dr;
	}

	protected void processDiffString(DiffValue diffValue, DiffResult dr) {
		DiffMatchPatch dmp = new DiffMatchPatch();
		List<Diff> diffs = dmp.diffMain(diffValue.getLeftValue(), diffValue.getRightValue());

		int numberOfDifferences = dmp.diffLevenshtein((LinkedList<Diff>) diffs);
		if (numberOfDifferences == 0) {
			dr.setValueMatchResultMessage(DiffConstants.CONTENT_LEFT_EQUAL_RIGHT);
		} else {
			dr.setValueMatchResultMessage(DiffConstants.CONTENT_LEFT_NOT_EQUAL_RIGHT);
			processTextDifferences(dr, diffs);
		}
	}

	protected void processTextDifferences(DiffResult dr, List<Diff> diffs) {
		int leftTextIndex = 0;
		dr.setSameSizeCharacterDifferences(new ArrayList<>());
		for (int i = 0; i < diffs.size(); i++) {
			Diff diff = diffs.get(i);
			if (!diff.operation.equals(Operation.EQUAL)) {
				Diff nextDiff = diffs.get(i + 1);

				StringBuilder sb = new StringBuilder("Right text differs from left at position ")
						.append(leftTextIndex + 1).append(" where left text value is '").append(diff.text)
						.append("' and right text is '").append(nextDiff.text).append("'");

				dr.getSameSizeCharacterDifferences().add(sb.toString());
				i++;
			}
			leftTextIndex += diff.text.length();
		}
	}

}
