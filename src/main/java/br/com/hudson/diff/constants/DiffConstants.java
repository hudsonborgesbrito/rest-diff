package br.com.hudson.diff.constants;

public class DiffConstants {
	public static final String LEFT= "Left";
	public static final String RIGHT= "Right";
	public static final String CONTENT_LEFT_EQUAL_RIGHT="Left text is equal Right text";
	public static final String CONTENT_LEFT_NOT_EQUAL_RIGHT="Left text is not equal Right text";
	public static final String SIZE_LEFT_EQUAL_RIGHT="Left text length is equal Right text length";
	public static final String SIZE_LEFT_LOWER_RIGHT="Left text length is lower Right text length";
	public static final String SIZE_LEFT_HIGHER_RIGHT="Left text length is higher Right text length";
}
