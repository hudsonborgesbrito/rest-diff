package br.com.hudson.diff.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("br.com.hudson.diff")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
