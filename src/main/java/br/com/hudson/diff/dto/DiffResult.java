package br.com.hudson.diff.dto;

import java.util.List;

public class DiffResult {

	private String sizeResultMessage;
	private String valueMatchResultMessage;
	
	private List<String> sameSizeCharacterDifferences;

	public String getSizeResultMessage() {
		return sizeResultMessage;
	}

	public void setSizeResultMessage(String sizeResultMessage) {
		this.sizeResultMessage = sizeResultMessage;
	}

	public String getValueMatchResult() {
		return valueMatchResultMessage;
	}

	public void setValueMatchResultMessage(String valueMatchResultMessage) {
		this.valueMatchResultMessage = valueMatchResultMessage;
	}

	public List<String> getSameSizeCharacterDifferences() {
		return sameSizeCharacterDifferences;
	}

	public void setSameSizeCharacterDifferences(List<String> sameSizeCharacterDifferences) {
		this.sameSizeCharacterDifferences = sameSizeCharacterDifferences;
	}

}
