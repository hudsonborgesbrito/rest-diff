#rest-diff

## How to run application
- Import the rest-diff application as a Maven project to your IDE.
- Run maven install command to build the project.
- Run br.com.hudson.diff.app.Application.java class as a java application.

## APIs: localhost:8080
### /v1/diff/<ID>/left
- Method: POST
- Param: Encoded Base64 body
- Response
    - 200 in case of success
    - 400 in case of error with the error message as the response body.

### /v1/diff/<ID>/right
- Method: POST
- Param: Encoded Base64 body
- Response
    - 200 in case of success
    - 400 in case of error with the error message as the response body.
    
### /v1/diff/<ID>
- Method: GET
- Response
    - 200 in case of success
        - Response parameters:
           - sizeResultMessage: String
         - valueMatchResultMessage: String
         - sameSizeCharacterDifferences: String[]
    - 400 in case of error with the error message as the response body.
     

